#!/bin/bash

sudo apt-get update 

echo "Installing zsh shell"
sudo apt-get install zsh
chsh -s /bin/zsh
echo "Installing apache2"
sudo apt-get install apache2sudo apt-get install mysql-server
sudo mkdir ~/Sites
sudo sed -i "$ a\<Directory $HOME/Sites>" /etc/apache2/apache2.conf
sudo sed -i "$ a\    Options Indexes FollowSymLinks" /etc/apache2/apache2.conf
sudo sed -i "$ a\    AllowOverride None" /etc/apache2/apache2.conf
sudo sed -i "$ a\    Require all granted" /etc/apache2/apache2.conf
sudo sed -i "$ a\</Directory>" /etc/apache2/apache2.conf

sudo a2enmod rewrite

echo "Installing google chrome"
sudo apt-get install libxss1 libappindicator1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
sudo rm *.deb /opt


echo "Installing mysql"
sudo apt-get install mysql-client

echo "Enter MySql root password"
mysql -u root -p <adduser

echo "Installing MySql WorkBench"
sudo apt-get install mysql-workbench

echo "Installing libapache2-mod-php5"
sudo apt-get install php5 libapache2-mod-php5
sudo apt-get install php5-curl
sudo apt-get install php5-mysql
sudo /etc/init.d/apache2 restart

echo "Installing nodejs"
sudo apt-get install nodejs

echo "Installing NPM"
sudo apt-get install npm

echo "Installing JDK"
sudo apt-get install default-jdk

echo "Installing Skype"
sudo apt-get install skype

echo "Installing Pidgin"
sudo apt-get install pidgin

echo "Installing Composer"
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

echo "Installing Bower"
sudo npm install -g bower

echo "Installing Ionic" 
sudo npm install -g ionic

echo "Installing Gulp"
sudo npm install -g gulp

echo "Installing cordova"
sudo npm install -g cordova

echo "Installing TOR browser"
sudo apt-get install tor

echo "Installing Redis"
cd ~/Downloads
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
sudo make install
make test
sudo mv ~/Downloads/redis-stable /opt

sudo apt-get install php5-xdebug
sudo sed -i "$ a\    xdebug.profiler_output_dir = \"/tmp/xdebug/\"" /etc/php5/apache2/php.ini
sudo sed -i "$ a\    xdebug.profiler_enable = On" /etc/php5/apache2/php.ini
sudo sed -i "$ a\    xdebug.remote_enable=On" /etc/php5/apache2/php.ini
sudo sed -i "$ a\    xdebug.remote_host=\"signingday\"" /etc/php5/apache2/php.ini
sudo sed -i "$ a\    xdebug.remote_port=9000" /etc/php5/apache2/php.ini
sudo sed -i "$ a\    xdebug.remote_handler=\"dbgp\"" /etc/php5/apache2/php.ini
sudo sed -i "$ a\    zend_extension=/usr/lib/php5/20131226/xdebug.so/xdebug.so" /etc/php5/apache2/php.ini
php --version
